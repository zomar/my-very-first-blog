<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){

    	$motto = 'Welcome to the page looser!!';
    	//return view('pages/index', compact('motto'));
    	return view('pages/index')->with('motto', $motto);
    }

    public function about(){

    	return view('pages/about');
    }
    
    public function services(){

    	return view('pages/services');
    }
}
