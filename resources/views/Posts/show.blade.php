@extends('layout.app')

@section('content')
	<a href="/posts" class="btn btn-light">Go Back</a>
	<h1>{{ $post->title }}</h1>
	<br>

	<h1>{!! $post->body !!}</h1>
	<hr>
	<small>Written on {{$post->created_at}}</small>
@endsection
