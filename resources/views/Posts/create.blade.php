@extends('layout.app')

@section('content')
	
	<h1>Create Post</h1>

	{!! Form::open(['action'=>'PostsController@store', 'method'=>'post']) !!}
    <div class="form-group">
		{!! Form::label('title', 'Title') !!}
		{!! Form::text('title', '', ['class'=>'form-control', 'placeholder'=>'title']) !!}
	</div>
		<br>
	<div class="form-group">
		{!! Form::label('body', 'Body') !!}
		{!! Form::textarea('body', '', ['id'=>'summary-ckeditor', 'class'=>'form-control', 'placeholder'=>'body']) !!}
	</div>
		<br>
	{!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
{!! Form::close() !!}
	
@endsection
