<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

<link rel="stylesheet" href="{{asset('css/app.css')}}">

    </head>
    
    <body>

        @include('includes/navbar')
        <br>

		<div class="container">
            @include('/includes/messages')
		   	@yield('content')
		</div>
        <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
        <script>
            CKEDITOR.replace( 'summary-ckeditor' );
        </script>
    </body>
</html>
